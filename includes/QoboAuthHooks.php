<?php

namespace MediaWiki\Extension\QoboAuth;

class QoboAuthHooks {
    public static function onRegistration() {
        $GLOBALS['wgPluggableAuth_Config'] = [
            "login" => [
                'plugin' => 'QoboAuth',
                'buttonLabelMessage' => 'Log in with Qobo account',
            ]
        ];
    }
}