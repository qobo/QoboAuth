<?php

namespace MediaWiki\Extension\QoboAuth;

use MediaWiki\Auth\AuthManager;
use MediaWiki\User\UserIdentity;
use MediaWiki\MediaWikiServices;
use MediaWiki\Extension\PluggableAuth\PluggableAuth;
use MediaWiki\Extension\PluggableAuth\PluggableAuthLogin;
use MWException;

// NOTE: this is a shit prototype.
// Qobo accounts will likely be made separate from Qobo itself
// so that we won't need to do this weird fucking call-another-backend
// shit. -Bluffingo 11/22/2023

class QoboAuth extends PluggableAuth {

    private $openSBPath;
    private $SB;
    private $DB;

    public function deauthenticate( UserIdentity &$user ): void
    {
    }

    public function saveExtraAttributes( $id ): void
    {
    }

    public static function getExtraLoginFields(): array
    {
        return [
            "username" => [
                'type' => 'string',
                'label' => wfMessage( 'userlogin-yourname' ),
                'help' => wfMessage( 'authmanager-username-help' ),
            ],
            "password" => [
                'type' => 'password',
                'label' => wfMessage( 'userlogin-yourpassword' ),
                'help' => wfMessage( 'authmanager-password-help' ),
                'sensitive' => true,
            ]
        ];
    }

    public function authenticate( &$id, &$username, &$realname, &$email, &$errorMessage ): bool
    {
        $config = MediaWikiServices::getInstance()->getMainConfig();
        $this->openSBPath = $config->get( 'OpenSBInstance' );

        $this->SB = $this->init_opensb();
        $this->DB = $this->SB->getBettyDatabase();

        $authManager = MediaWikiServices::getInstance()->getAuthManager();
        $extraLoginFields = $authManager->getAuthenticationSessionData(
            PluggableAuthLogin::EXTRALOGINFIELDS_SESSION_KEY
        );

        $username = $extraLoginFields["username"];
        $password = $extraLoginFields["password"];

        $openSBUser = $this->get_opensb_user($username, $password);

        if (!$openSBUser) {
            $errorMessage = "Either your credentials are invalid, or you must register an account on Qobo.";
        } else {
            $openSBUserID = $this->DB->fetch("SELECT id FROM users WHERE name = ?", [$username]);
            if ($this->DB->fetch("SELECT * FROM bans WHERE userid = ?", [$openSBUserID])) {
                $errorMessage = "You are banned from Qobo and cannot proceed.";
            } else {
                return true;
            }
        }

        return false;
    }

    private function init_opensb() {
        define('SB_MEDIAWIKI', true);

        if (!is_dir($this->openSBPath)) {
            throw new MWException( 'The path to the Qobo/openSB instance is invalid.' );
        } else {
            require_once($this->openSBPath . "private/class/common.php");
        }

        if(!$betty) {
            throw new MWException( "OpenSB's Betty variable has not been defined." );
        }

        return $betty;
    }

    private function get_opensb_user($username, $password) {
        $logindata = $this->DB->fetch("SELECT password,name FROM users WHERE name = ?", [$username]);

        if ($logindata["name"]) {
            if (password_verify($password, $logindata['password'])) {
                return true;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}